<?php

declare(strict_types=1);

namespace CoStack\LibTests\Unit\Pattern\Double;

use CoStack\Lib\Pattern\Singleton;

class ClassWithSingletonTrait
{
    use Singleton;
}
