<?php

declare(strict_types=1);

namespace CoStack\Lib\Exceptions;

use Exception;

abstract class LibException extends Exception
{
}
